```bash
$ git clone git@gitlab.com:krishnarao_inturi/bezierjs-project.git
$ cd bezierjs-project
```

\$ Install dependencies:

```bash
   $ npm install (only in development)

   $ npm install --only=prod(use in demo environment or in prod.. dont use npm install)
```

Start the server:

```bash
   $ npm start
```
