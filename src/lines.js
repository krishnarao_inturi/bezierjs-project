export const intersect = (x1, y1, x2, y2, x3, y3, x4, y4) => {
  // if the lines intersect, the result contains the x and y of the intersection (treating the lines as infinite) and booleans for whether line segment 1 or line segment 2 contain the point
  const denominator = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1);
  if (denominator == 0) {
    return {
      x: null,
      y: null,
    };
  }
  let a = y1 - y3;
  let b = x1 - x3;
  const numerator1 = (x4 - x3) * a - (y4 - y3) * b;
  const numerator2 = (x2 - x1) * a - (y2 - y1) * b;
  a = numerator1 / denominator;
  b = numerator2 / denominator;

  // if we cast these lines infinitely in both directions, they intersect here:
  return {
    x: x1 + a * (x2 - x1),
    y: y1 + a * (y2 - y1),
  };
};
