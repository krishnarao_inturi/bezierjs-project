import bezierJS from "bezier-js";
import clone from "clone";
import curvify from "curvify-svg-path";
import parse from "parse-svg-path";
import toAbs from "abs-svg-path";
import serialize from "serialize-svg-path";
import { intersect } from "./lines";

//global-ish variable for enabling / disabling the points
let drawpoints;

//convert bezierjs object to array-like object
const bezierToArray = (b) => {
  let end = [];
  let point = [];
  for (let i = 0; i < b.points.length; i++) {
    const p = b.points[i];
    if (i === 0) {
      end.push(p.x);
      end.push(p.y);
    } else {
      if (!point.length) {
        point.push("C");
      }
      point.push(p.x);
      point.push(p.y);
    }
  }

  return {
    end: end,
    point: point,
  };
};

//get the outlined lines
const outlinePoints = (a, b, distance) => {
  let startX, startY;
  if (a.length === 3) {
    startX = a[1];
    startY = a[2];
  } else {
    startX = a[5];
    startY = a[6];
  }
  //make a new bezier
  let bezier = new bezierJS(startX, startY, b[1], b[2], b[3], b[4], b[5], b[6]);

  //if its a straight line, we have to set the handles in the center.
  //else bezierjs breaks.
  let straight;
  if (startX === b[1] && startY === b[2]) {
    const c = bezier.get(0.5);
    b[1] = c.x;
    b[2] = c.y;

    straight = true;
  }

  if (b[3] === b[5] && b[4] === b[6]) {
    const c = bezier.get(0.5);
    b[3] = c.x;
    b[4] = c.y;

    straight = true;
  }

  if (straight) {
    //make a new curve with the updated points
    bezier = new bezierJS(startX, startY, b[1], b[2], b[3], b[4], b[5], b[6]);
  }

  let t = bezier.offset(-distance);
  const array = bezierToArray(t[0]);

  const points = [];
  points[0] = ["M", array.end[0], array.end[1]];
  points[1] = array.point;

  return points;
};

const getSlope = (x1, y1, x2, y2, reverse) => {
  let slope;
  if (y1 >= y2 && x1 >= x2) {
    slope = Math.atan((y2 - y1) / (x2 - x1));
    if (reverse) {
      slope = Math.tan(y2 - y1, x2 - x1);
    }
  } else if (y1 >= y2 && x1 <= x2) {
    slope = Math.atan((y2 - y1) / (x1 - x2));
    if (reverse) {
      slope = Math.tan(y2 - y1, x1 - x2);
    }
  } else if (y1 <= y2 && x1 <= x2) {
    slope = Math.atan((y1 - y2) / (x1 - x2));
    if (reverse) {
      slope = Math.tan(y1 - y2, x1 - x2);
    }
  } else if (y1 <= y2 && x1 >= x2) {
    slope = Math.atan((y1 - y2) / (x2 - x1));
    if (reverse) {
      slope = Math.tan((y1 - y2) / (x2 - x1));
    }
  }

  return slope;
};

const getHandle = (x1, y1, x2, y2, slope, radius, reverse) => {
  //approximation for cubic curve
  //see http://spencermortensen.com/articles/bezier-circle/
  const approx = (radius / 2) * (1 - 0.55191502449);

  let p = {};
  if (x1 === x2 && y1 < y2) {
    p.x = x2 + slope * approx;
    p.y = y2;
    if (reverse) {
      p.x = x2 - slope * approx;
      p.y = y2;
    }
  } else if (x1 < x2 && y1 === y2) {
    p.x = x2;
    p.y = y2 + slope * approx;
    if (reverse) {
      p.x = x2;
      p.y = y2 - slope * approx;
    }
  } else if (x1 > x2 && y1 === y2) {
    p.x = x2;
    p.y = y2 - slope * approx;
    if (reverse) {
      p.x = x2;
      p.y = y2 + slope * approx;
    }
  } else if (x1 === x2 && y1 > y2) {
    p.x = x2 - slope * approx;
    p.y = y2;
    if (reverse) {
      p.x = x2 + slope * approx;
      p.y = y2;
    }
  } else if (x1 < x2 && y1 < y2) {
    p.x = x2 + approx;
    p.y = y2 + slope * approx;
    if (reverse) {
      p.x = x2 - approx;
      p.y = y2 - slope * approx;
    }
  } else if (x1 < x2 && y1 > y2) {
    p.x = x2 + approx;
    p.y = y2 - slope * approx;
    if (reverse) {
      p.x = x2 - approx;
      p.y = y2 + slope * approx;
    }
  } else if (x1 > x2 && y1 > y2) {
    p.x = x2 - approx;
    p.y = y2 - slope * approx;
    if (reverse) {
      p.x = x2 + approx;
      p.y = y2 + slope * approx;
    }
  } else if (x1 > x2 && y1 < y2) {
    p.x = x2 - approx;
    p.y = y2 + slope * approx;
    if (reverse) {
      p.x = x2 + approx;
      p.y = y2 - slope * approx;
    }
  }

  return p;
};

const strokeLinejoin = (d, cap, distance) => {
  let type = "line";

  //if last point is same as first point, its a shape
  if (d[d.length - 1][5] === d[0][1] && d[d.length - 1][6] === d[0][2]) {
    type = "shape";
  }

  //outline the lines
  const points = [];
  for (let i = 1; i < d.length; i++) {
    const getPoints = outlinePoints(d[i - 1], d[i], distance);
    points.push(getPoints[0]);
    points.push(getPoints[1]);
  }

  //connect the points (make a bevel)
  for (let i = 1; i < points.length; i++) {
    if (points[i][0] === "M") {
      points[i][0] = "C";
      points[i][3] = points[i][1];
      points[i][4] = points[i][2];
      points[i][5] = points[i][1];
      points[i][6] = points[i][2];

      points[i][1] = points[i - 1][5];
      points[i][2] = points[i - 1][6];
    }
  }

  //add the last line
  if (type === "shape") {
    const l = points.length;
    points[l] = [
      "C",
      points[l - 1][5],
      points[l - 1][6],
      points[0][1],
      points[0][2],
      points[0][1],
      points[0][2],
    ];
  }

  //get the mitter, doesnt work yet
  //but should be a lot easier then cap `round`
  if (cap === "mitter") {
    //get all the endpoints for miter
    let endpoints = [];
    for (let i = 1; i < points.length; i++) {
      if (i % 2 === 1) {
        let x1, y1;
        if (i === 1) {
          x1 = points[0][1];
          y1 = points[0][2];
        } else {
          x1 = points[i - 1][5];
          y1 = points[i - 1][6];
        }
        let x2 = points[i][5];
        let y2 = points[i][6];

        let x3, y3, x4, y4;
        if (i === points.length - 1) {
          x3 = points[0][1];
          y3 = points[0][2];
          x4 = points[1][5];
          y4 = points[1][6];
        } else if (i === points.length - 2) {
          x3 = points[1][5];
          y3 = points[1][6];
          x4 = points[i + 1][5];
          y4 = points[i + 1][6];
        } else {
          x3 = points[i + 1][5];
          y3 = points[i + 1][6];
          x4 = points[i + 2][5];
          y4 = points[i + 2][6];
        }

        const intersection = intersect(x1, y1, x2, y2, x3, y3, x4, y4);

        endpoints.push(intersection);
      }
    }

    console.log(endpoints);
  }

  //round the linejoin
  if (cap === "round") {
    //round every second line
    const pClone = clone(points);
    for (let i = 1; i < pClone.length; i++) {
      if (type === "line") {
        if (i === points.length - 1) {
          continue;
        }
      }
      if (i % 2 === 1) {
        //first line
        let x1, y1;
        if (i === 1) {
          x1 = pClone[i - 1][1];
          y1 = pClone[i - 1][2];
        } else {
          x1 = pClone[i - 1][5];
          y1 = pClone[i - 1][6];
        }
        let x2 = pClone[i][5];
        let y2 = pClone[i][6];

        //second line
        let x3, y3, x4, y4;
        if (i === pClone.length - 2) {
          x3 = pClone[0][1];
          y3 = pClone[0][2];
          x4 = pClone[1][5];
          y4 = pClone[1][6];
        } else {
          x3 = pClone[i + 1][5];
          y3 = pClone[i + 1][6];
          x4 = pClone[i + 2][5];
          y4 = pClone[i + 2][6];
        }

        //get the degrees (slope) of the 2 lines
        const a1 = getSlope(x1, y1, x2, y2);
        const a2 = getSlope(x3, y3, x4, y4);

        if (i === 3) {
          //drawPoints({ x: x1, y: y1 }, 'round')
          //drawPoints({ x: x2, y: y2 }, 'round')
          //drawPoints({ x: x3, y: y3 }, 'round')
          //drawPoints({ x: x4, y: y4 }, 'round')
        }

        //get the degrees (slope) of the center
        const a = (a1 + a2) / 2;

        //get the original shape point
        const j = (i + 1) / 2;
        const x = d[j][5];
        const y = d[j][6];

        //calc the point where the rounding will end
        const ax = distance * Math.cos(a) + x;
        const ay = distance * Math.sin(a) + y;

        /****************************************
        //depending on the x/y direction the path is drawn differently
        //how to figure out which direction it will be drawn!!!??
        //for now.. a mystery.
        *****************************************/
        const slope1 = -getSlope(x, y, x2, y2);
        const slope2 = getSlope(x, y, ax, ay, true);
        const slope4 = -getSlope(x, y, x3, y3);

        const h1 = getHandle(x, y, x2, y2, slope1, distance);
        const h2 = getHandle(x, y, ax, ay, slope2, distance);
        const h3 = getHandle(x, y, ax, ay, slope2, distance, true);
        const h4 = getHandle(x, y, x3, y3, slope4, distance);

        console.log(x);
        console.log(x3);

        //right now this only works for the 3rd point
        if (i === 3) {
          points[i + 1][1] = h3.x;
          points[i + 1][2] = h3.y;
          points[i + 1][3] = h4.x;
          points[i + 1][4] = h4.y;
          points.splice(i + 1, 0, ["C", h1.x, h1.y, h2.x, h2.y, ax, ay]);

          //drawPoints({ x: h1.x, y: h1.y }, "round");
          drawPoints({ x: h4.x, y: h4.y }, "round");
          drawPoints({ x: x3, y: y3 }, "round");
          drawPoints({ x: x, y: y }, "round");
        }
      }
    }
  }

  return points;
};

//draw points on x / y.
//works with a parsed d attribute or just {x:0,y:0}
//id is either `round` or `bevel`, it determines in which shape the point is drawn
const drawPoints = (d, id) => {
  if (!drawpoints) {
    return;
  }
  if (d.x && d.y) {
    const point = document.createElementNS(
      "http://www.w3.org/2000/svg",
      "circle"
    );
    point.setAttribute("cx", d.x);
    point.setAttribute("cy", d.y);
    point.setAttribute("fill", "green");
    point.setAttribute("r", 3);
    point.setAttribute("class", "cpoint");
    document.getElementById(id).parentElement.appendChild(point);
    return;
  }
  for (let i = 0; i < d.length; i++) {
    let x, y;
    if (i === 0) {
      x = d[0][1];
      y = d[0][2];
    } else {
      x = d[i][5];
      y = d[i][6];
    }

    const point = document.createElementNS(
      "http://www.w3.org/2000/svg",
      "circle"
    );
    point.setAttribute("cx", x);
    point.setAttribute("cy", y);
    point.setAttribute("fill", "blue");
    point.setAttribute("r", 3);
    point.setAttribute("class", "cpoint");
    document.getElementById(id).parentElement.appendChild(point);
  }
};

//remove all points
const clearPoints = () => {
  const points = document.querySelectorAll(".cpoint");
  for (let i = 0; i < points.length; i++) {
    points[i].remove();
  }
};

//only works with a `normalized` path to `C`
const originalRound = document
  .getElementById("original-round")
  .getAttribute("d");
const roundPath = curvify(toAbs(parse(originalRound)));

const originalBevel = document
  .getElementById("original-bevel")
  .getAttribute("d");
const bevelPath = curvify(toAbs(parse(originalBevel)));

//outline the path, taking in account the linejoin
const makeLinejoin = (e) => {
  const w = e.target.value;
  const strokeW = w / 2;

  //remove all points previously drawn
  clearPoints();

  //bezel or round.
  const round = strokeLinejoin(clone(roundPath), "round", strokeW);
  const bevel = strokeLinejoin(clone(bevelPath), "bevel", strokeW);

  //drawPoints(round, "round");
  drawPoints(bevel, "bevel");

  const dRound = serialize(round);
  const dBevel = serialize(bevel);

  document.getElementById("original-round").setAttribute("stroke-width", w);
  document.getElementById("original-bevel").setAttribute("stroke-width", w);
  document.getElementById("round").setAttribute("d", dRound);
  document.getElementById("bevel").setAttribute("d", dBevel);
};

//input event for the stroke-width
const event = new Event("change");
const input = document.getElementById("input");
input.addEventListener("change", makeLinejoin);

//show / hide points
const showpoints = (e) => {
  if (e.target.checked) {
    drawpoints = true;
  } else {
    drawpoints = false;
  }
  input.dispatchEvent(event);
};

//input event for showing points
const event2 = new Event("change");
const input2 = document.getElementById("show-points");
input2.addEventListener("change", showpoints);
input2.dispatchEvent(event2);
